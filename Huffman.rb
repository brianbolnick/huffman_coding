require 'priority_queue'

#---------------------------------------------------------------------------------------------------#
def huffman_code(input)
  #1. Create hash to hold frequencies. Iterate over to calc frequency
  @sym_freq = Hash.new(0)
  input.each_char {|x|
    @sym_freq[x] += 1
  }

  #2. Create a priority queue to hold  values
  queue = CPriorityQueue.new

  #3. Iterate over hash and add values to pq
  @sym_freq.each {|char, count| queue.push(char, count)}

  #4. Combine frequencies until tree has been completed
  while queue.length > 1
    first_sym, first_freq = queue.delete_min
    second_sym,second_freq = queue.delete_min

    #combine chars to make new char, adds count of each to make sum count
    queue.push([first_sym, second_sym], first_freq + second_freq)
  end

  #5. calculate binary value for each character by iterating through queue
  Hash[*generate_encoding(queue.min_key)]
end

#---------------------------------------------------------------------------------------------------#
def generate_encoding(array, prefix='')
  # Calculate binary representation
  case array
    when Array
      #work with left child, right child, add 0 or 1 accordingly
      generate_encoding(array[0], "#{prefix}0") + generate_encoding(array[1], "#{prefix}1")
    else
      # Handle individual chars
      [array, prefix]
  end
end

#---------------------------------------------------------------------------------------------------#
def encode(input, encoding)
  #collect bin values for each char and combine into one string
  input.each_char.collect {|char| encoding[char]}.join
end

#---------------------------------------------------------------------------------------------------#
def create_new_freq
  #Create hash with values from pg 771 #27
  new_hash = {'A' => 0.0817, 'B' => 0.0145, 'C' => 0.0248, 'D' => 0.0431, 'E' => 0.1232, 'F' => 0.0209,
              'G' => 0.0182, 'H' => 0.0668,  'I' => 0.0689, 'J' => 0.0010, 'K' => 0.0080,'L' => 0.0397,
              'M' => 0.0277, 'N' => 0.0662, 'O' => 0.0781, 'P' => 0.0156, 'Q' => 0.0009, 'R' => 0.0572,
              'S' => 0.0628, 'T' => 0.0905, 'U' => 0.0304, 'V' => 0.0102, 'W' => 0.0264, 'X' => 0.0015,
              'Y' => 0.0211, 'Z' => 0.0005}

  # Create priority queue with hash values
  queue = CPriorityQueue.new
  new_hash.each {|char, count| queue.push(char, count)}

  # Combine chars in tree
  while queue.length > 1
    first_sym, first_freq = queue.delete_min
    second_sym,second_freq = queue.delete_min
    queue.push([first_sym, second_sym], first_freq + second_freq)
  end

  # calculate binary value for each character by iterating through queue
  Hash[*generate_encoding(queue.min_key)]
end
#---------------------------------------------------------------------------------------------------#
def create_bin
  #Create and write to bin file
  old_text = File.open('metamorphosis.txt').read # copy text from file
  bin_file = File.new('metamorphosis.bin', 'w+') # create binary file (writable)
  bin_file.puts(old_text.unpack('B*')[0]) #convert text to binary and write to file [unpack]
  bin_file.close # close the file
end

#---------------------------------------------------------------------------------------------------#
def display_size(file)
  File.open(file).size
end

#---------------------------------------------------------------------------------------------------#
#----------------------------------------MAIN-------------------------------------------------------#
text = File.open('metamorphosis.txt').read
puts '--------------------------------------------------------------------'
puts 'Huffman Coding Assignment:'

#1. Convert file to binary and measure the size
create_bin
txt_size = display_size('metamorphosis.txt')
bin_size = display_size('metamorphosis.bin')
puts '--------------------------------------------------------------------'
puts "Size of text file: #{txt_size} bytes"
puts '--------------------------------------------------------------------'
puts "Size of binary file: #{bin_size} bytes"
puts '--------------------------------------------------------------------'

#2. Implement huffman coding
encoding = huffman_code(text)
enc = encode(text, encoding)


#3. measure newly compressed bin file size
new_bin_file = File.new('metamorphosis_comp.bin', 'w+')
new_bin_file.puts(enc)
new_bin_file.close
comp_bin_size  = File.open('metamorphosis_comp.bin').size
puts "Size of compressed file = #{comp_bin_size}"
puts '--------------------------------------------------------------------'

#4. compare size differences between compressed and none compressed
percent = comp_bin_size.fdiv(bin_size)
puts "Huffman encoded bin file is #{bin_size - comp_bin_size} bytes less. That is about #{sprintf('%.0f',percent * 100)}% smaller."
puts '--------------------------------------------------------------------'

#5. Reimplement with with values from pg 771 #27
encoding1 = create_new_freq
enc1 = encode(text, encoding1)
new_bin_file_27 = File.new('metamorphosis_comp_27.bin', 'w+')
new_bin_file_27.puts(enc1)
new_bin_file_27.close
puts "File with values from pg 771 #27 size: #{display_size('metamorphosis_comp_27.bin')} bytes"
puts '--------------------------------------------------------------------'

#6. Compare values between frequencies
puts 'Byte values with actual frequencies:'
encoding.to_a.sort.each {|x| p x}
puts "--------------------------------------------------------------------\n"
puts 'Byte values with frequencies found on pg. 771 #27:'
encoding1.to_a.sort.each {|x| p x}
puts '--------------------------------------------------------------------'






